init:
	pip install -r requirements.txt
build:
	pip install -r requirements.txt
	pyinstaller --onefile multiGet.py
	ln -s dist/multiGet
clean:
	rm -rf dist build multiGet.spec
	rm multiGet
test:
	pip install -r requirements.txt
	python3 -m unittest discover tests