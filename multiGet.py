"""Downloads part of a file from a web server, in chunks.
Output file may be specified with a command-line option"""
import argparse
from http import HTTPStatus as status
import logging
import os
import requests

logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')


def multiGet(url, output_file=None, num_of_chunk=4, chunk_size=1048576):
    try:
        assert num_of_chunk > 0, "Invalid number of chunk"
        assert chunk_size > 0, "Invalid chunk size"

        #init index
        increment = chunk_size - 1 #adjusted indexing
        start = 0
        end = increment

        #default output file name
        if not output_file:
            output_file = url[url.rfind('/')+1:]

        file = open(output_file, "bw")
    except Exception as e:
        logging.error("{}".format(e))
        return False

    try:
        for i in range(0, num_of_chunk):
            r = requests.get(url, headers={'Range': 'bytes={}-{}'.format(start, end)})
            # logging.info ("Status {}".format(r.status_code))
            if r.status_code == status.PARTIAL_CONTENT or r.status_code == status.OK:
                file.write(r.content)
                start = end + 1
                end = start + increment
            elif r.status_code == status.PARTIAL_CONTENT or r.status_code == status.NO_CONTENT:
                break
    except Exception as e:
        logging.error("{}".format(e))
        if os.path.isfile(output_file):
            file.close()
            os.remove(output_file)

        return False
    finally:
        file.close()

    return True

if __name__ == "__main__":
    #We are not validate url.
    parser = argparse.ArgumentParser(description='Downloads part of a file from a web server, in chunks.')
    parser.add_argument('url', metavar='URL', type=str,
                        help='Source URL to be downloaded')
    parser.add_argument('-out_file', metavar='', type=str, help='Output file name; default to file in URL')
    parser.add_argument('-chunk_size', metavar='', type=int, help='Each chunk size to be download (byte)')
    parser.add_argument('-num_chunk', metavar='', type=int, help='Number of chunk to be download')

    args = parser.parse_args()
    params = {}
    if args.out_file is not None:
        params['output_file'] = args.out_file
    if args.chunk_size is not None:
        params['chunk_size'] = args.chunk_size
    if args.num_chunk is not None:
        params['num_of_chunk'] = args.num_chunk

    logging.info("Calling with {}".format(params))
    if not multiGet(args.url, **params):
        logging.warning("Download failed")
