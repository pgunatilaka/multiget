#MultiGet
An application that downloads part of a file from a web server, in chunks.


## Getting Started
### Prerequisites
[python3](https://www.python.org/downloads/)

## Installation
```
make build
```

## Running the tests

```
make test
```

## Usage
multiGet [-h] [-out_file] [-chunk_size] [-num_chunk] URL

Downloads part of a file from a web server, in chunks.

positional arguments:
  URL           Source URL to be downloaded

optional arguments:
  -h, --help    show this help message and exit
  -out_file     Output file name; default to file in URL
  -chunk_size   Each chunk size to be download (byte)
  -num_chunk    Number of chunk to be download


## Script mode
 multiGet app can be executed as a python script.

```
 pip install -r requirements.txt

 python3 multiGet.py <url>
```

