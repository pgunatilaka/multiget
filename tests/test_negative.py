import unittest
import os
import sys
from stat import S_IREAD
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
import multiGet as mg

class NegativeTestSuite(unittest.TestCase):
    """Testing invalid conditions, file is not download."""
    files_created=[]
    default_url = "http://cecfbddf4927529.bwtest-aws.pravala.com/384MB.jar"
    default_out = "384MB.jar"

    def setUp(self):
        self.files_created = [self.default_out]

    def tearDown(self):
        for f in self.files_created:
            if os.path.isfile(f):
                os.remove(f)

    def test_invalid_output_file_name(self):
        self.assertFalse(mg.multiGet(self.default_url, output_file=".///"))

    def test_negative_chunk_num(self):
        self.assertFalse(mg.multiGet(self.default_url, num_of_chunk=-10))
        self.assertFalse(os.path.isfile(self.default_out))

    def test_invalid_url(self):
        self.assertFalse(mg.multiGet('bah'))
        self.assertFalse(os.path.isfile(self.default_out))


    def test_download_to_readonly(self):
        out_file = "readonly"
        self.files_created.append(out_file)
        mg.multiGet(self.default_url, output_file="readonly", chunk_size=4)
        os.chmod(out_file, S_IREAD)
        self.assertFalse(mg.multiGet(self.default_url, output_file="readonly", chunk_size=8))
        self.assertEqual(os.path.getsize(out_file), 16)


if __name__ == '__main__':
    unittest.main()
