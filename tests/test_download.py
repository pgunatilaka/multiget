import unittest
import sys
import os
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
import multiGet as mg

class DownloadTestSuite(unittest.TestCase):
    """Download test cases."""
    files_created=[]

    def setUp(self):
        self.files_created = []

    def tearDown(self):
        for f in self.files_created:
            if os.path.isfile(f):
                os.remove(f)

    def test_image_file(self):
        out_file = "googlelogo_color_272x92dp.png"
        self.files_created.append(out_file)
        url = "http://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png"
        self.assertTrue(mg.multiGet(url, chunk_size=3376, num_of_chunk=5))
        self.assertTrue(os.path.isfile(out_file))
        self.assertEqual(os.path.getsize(out_file), 13504)

    def test_pdf_file(self):
        out_file = "CarnegieCodingCheckMultiGet.pdf"
        self.files_created.append(out_file)
        url = "http://dist.pravala.com/coding/CarnegieCodingCheckMultiGet.pdf"
        self.assertTrue(mg.multiGet(url, chunk_size=10000000000))
        self.assertTrue(os.path.isfile(out_file))
        self.assertEqual(os.path.getsize(out_file), 689380)


    def test_gzip_file(self):
        out_file = "my_file.zip"
        self.files_created.append(out_file)
        url = "http://dist.pravala.com/coding/multiGet-example.zip"
        self.assertTrue(mg.multiGet(url, num_of_chunk=1, chunk_size=8, output_file=out_file))
        self.assertTrue(os.path.isfile(out_file))
        self.assertEqual(os.path.getsize(out_file), 8)


if __name__ == '__main__':
    unittest.main()
