import unittest
import sys
import os
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
import multiGet as mg

class DownloadTestSuite(unittest.TestCase):
    """Default download test cases."""
    files_created=[]
    default_url = "http://cecfbddf4927529.bwtest-aws.pravala.com/384MB.jar"
    default_out = "384MB.jar"

    def setUp(self):
        self.files_created = [self.default_out]

    def tearDown(self):
        for f in self.files_created:
            if os.path.isfile(f):
                os.remove(f)

    def test_basic_download(self):
        out_file = self.default_out
        mg.multiGet(self.default_url)
        self.assertTrue(os.path.isfile(out_file))
        self.assertEqual(os.path.getsize(out_file), 4194304)

    def test_download_with_filename(self):
        out_file = "small_file.jar"
        self.files_created.append(out_file)
        mg.multiGet(self.default_url, output_file=out_file)
        self.assertTrue(os.path.isfile(out_file))
        self.assertEqual(os.path.getsize(out_file), 4194304)

    def test_download_smaller_chunk(self):
        out_file = self.default_out
        mg.multiGet(self.default_url, chunk_size=1)
        self.assertTrue(os.path.isfile(out_file))
        self.assertEqual(os.path.getsize(out_file), 4)

    def test_download_more_chunk(self):
        out_file = self.default_out
        mg.multiGet(self.default_url, num_of_chunk=10)
        self.assertTrue(os.path.isfile(out_file))
        self.assertEqual(os.path.getsize(out_file), 10485760)

    def test_download_chunks_gt_file_size(self):
        out_file = self.default_out
        mg.multiGet(self.default_url,  num_of_chunk=1000)
        self.assertTrue(os.path.isfile(out_file))
        self.assertEqual(os.path.getsize(out_file), 402653352)

    def test_download_all(self):
        out_file = 'out_file'
        self.files_created.append(out_file)
        mg.multiGet(self.default_url,  num_of_chunk=2, output_file=out_file, chunk_size=600)
        self.assertTrue(os.path.isfile(out_file))
        self.assertEqual(os.path.getsize(out_file), 1200)

if __name__ == '__main__':
    unittest.main()


